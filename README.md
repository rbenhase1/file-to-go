# File to Go #
Contributors: rbenhase
Tags: uploads
Requires at least: 4.3
Tested up to: 4.3
Stable tag: 4.3
License: WTFPL
License URI: http://www.wtfpl.net/about/

Allows you to upload and replace files at user-defined URLs, giving you the ability to publish updated versions of files without having to change out URLs all over your site.

## Description ##

This plugin works by allowing you to create one or more file/link pairs in an administrative interface. First, you upload a file or choose one from your media library. Next,
you specify the link to use. Once you've set a link, you can then replace the file (e.g. to provide an updated version without having to change the link or manually replace 
the file on your server). You can also change the link, but note that this will require you to update any pages on your site that use the link.

By default, this plugin uses `/files/` as the base of its custom links (e.g. `http://example.com/files/yourfile.pdf`). You can change this base to something else, but it's 
really not recommended once you've added files/links (as it will force you to have to update every instance of each link on your site).

## Installation and Usage ##

1. Upload the contents of this directory (`file-to-go`) to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click on "File to Go" in your admin menu to begin adding links.