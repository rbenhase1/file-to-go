/*
 * file-to-go.js
 */
(function($){
  'use strict';
  
  // Instantiates the variable for the media library frame.
  var file_frame;
  var elem;
  var original;
      
  $(document).ready(function(){      
    
    $(".form-table, #submit").hide();
    
    $(".do-anyway").click(function() {
      $(this).slideUp(200);
      $(".form-table, #submit").slideDown(200);
    });
    
    $("#submit").click(function(e){
      var confirmation = confirm("This will immediately break any existing links. Are you really sure you want to change the link base?");
                          
      if (confirmation !== true) {
        e.preventDefault();
      }

    });
    
    // Dynamically show/hide some specific elements
    var w = $("td.link").first().outerWidth() + 10;
    $(".display-link").show();
    $(".edit-link").hide(0, function(){
      $("td.link").width(w);
      $(".display-link").show();
    });
    
  
    $(document).on("click", ".edit-link-slug", function(e) {        
      e.preventDefault();
      
      var elem = $(this);   
      var w = elem.width();
      
      elem.removeClass("edit-link-slug button-secondary")
                .addClass("set-link button-primary")
                .text("Set Link")
                .css("text-align", "center")
                .width(w);
                
      elem.parent().parent().find("td .display-link").toggle(0, function() {
              $(this).parent().find(".edit-link").toggle();
            });
      
    });

    
    $(document).on("click", ".set-link", function(e) {     

      e.preventDefault();      
      var elem = $(this);     
         
      var newSlug = elem.parent().parent().find("td input.link-slug").val();
      var file = elem.parent().parent().find("td a.original-file").attr("href");
      var originalValue = elem.data("saved-value");

      if (newSlug !== originalValue ) {
        
        var userInput = false;
        
        if (originalValue != undefined) {
          var userInput = confirm("Warning: Changing a link means that you will need to edit any pages on your site that use the old link (" + 
                          Options.siteUrl + '/' + Options.base + '/' + originalValue + ") and update them to use the new link. Are you sure you wish to proceed?");
        }               
        if (userInput == true || originalValue == undefined) {
          $.post(
            ajaxurl,
            {
              action: 'change_link',
              newSlug: newSlug == undefined ? '' : newSlug,
              originalValue: originalValue == undefined ? '' : originalValue,
              file: file  == undefined ? '' : file
            },
            function(response) {
              if (response.success === true) {
                
                elem.parent().parent().css("background-color", "#CCFFFF").find("td:last").css("background-color", "#EEEEEE");
                
                elem.removeClass("set-link button-primary")
                    .addClass("edit-link-slug button-secondary")
                    .text("Change Link");
                var newLink = Options.siteUrl + '/' + Options.base + '/' + response.slug;
                elem.parent().parent().find("td .display-link a").text(newLink).attr("href", newLink);
                elem.parent().parent().find("td .display-link").toggle(0, function() {
                  $(this).parent().find(".edit-link").toggle();
                });
                elem.data("saved-value", newSlug);
              } else {
                alert(response.message);
              }
          });
        }
      }
    });

    // Runs when the image button is clicked.
    $(document).on('click', '.files-to-go-add-file, .replace-existing-file', function(e){

      e.preventDefault();
      elem = $(this);      
      original = $(this).closest("tr").find(".original-file");
      
      // If the frame already exists, re-open it.
      if ( undefined !== file_frame ) {
         file_frame.open();
         return;
      }
      // Sets up the media library frame
      file_frame = wp.media.frames.file_frame = wp.media({
          title: "Select a File",
          button: { text:  "Use This File" },
          multiple: false
      });

      // Runs when a file is selected.
      file_frame.on('select', function(){

          // Grabs the attachment selection and creates a JSON representation of the model.
          var media_attachment = file_frame.state().get('selection').first().toJSON();        
          var fileUrl = media_attachment.url;
          var slug = elem.parent().parent().find(".link-slug").val();
          
          if (slug != undefined) {
            
            $.post(
            ajaxurl,
            {
              action: 'update_file',
              fileUrl: fileUrl,
              slug: slug
            },
            function(response) {
              if (response.success === true) {

                // Sends the attachment URL to our custom image input field.
                original.attr("href",fileUrl).text(fileUrl.substring(fileUrl.lastIndexOf('/') + 1));

                // If we're adding a file for the first time.
                if (elem.hasClass("files-to-go-add-file")) {
                
                  var actionsTd = elem.closest("tr").find("td.actions");
    
    
                  var linkPrefix = $("<span></span>")
                        .text(Options.siteUrl + '/' + Options.base + '/');
                  
                  var newLink = $("<input>")
                        .attr("type", "text")
                        .addClass("link-slug")
                        .attr("name", "file-to-go-urls[" + media_attachment.url + "]");
                        
                  elem.closest("tr").find(".display-link")
                      .hide();
                        
                  elem.closest("tr").find(".edit-link")
                      .html(linkPrefix).append(newLink)
                      .show(); 
                      
                  var replaceFile = elem.text("Replace File").removeClass("files-to-go-add-file").addClass("replace-existing-file");
                  
                  elem.remove();
                                
                  var setLink = $("<a></a>")
                        .addClass("set-link button-primary")
                        .attr("href", "#")
                        .text("Set This Link");
                  
                  actionsTd.html(setLink).append(' ', replaceFile);
                  
                  elem.parent().parent().find(".link-slug").focus();
                
                } 
              } else {
                alert(response.message);
              }
          });
        }
        return false;
      });

      // Opens the media library frame.
      file_frame.open();
    });
  });
})(jQuery);