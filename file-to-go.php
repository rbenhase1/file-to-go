<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              http://2060digital.com
 * @since             1.0.0
 * @package           File_To_Go
 *
 * @wordpress-plugin
 * Plugin Name:       File To Go
 * Plugin URI:        http://2060digital.com
 * Description:       Allows you to upload and replace files at user-defined URLs, giving you the ability to publish updated versions of files without having to change out URLs all over your site.
 * Version:           1.0.0
 * Author:            Ryan Benhase
 * Author URI:        http://2060digital.com
 * License:           WTFPL
 * License URI:       http://www.wtfpl.net/txt/copying/
 * Text Domain:       file-to-go
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-file-to-go.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_file_to_go() {

	$plugin = new File_To_Go();
	$plugin->run();

}
run_file_to_go();
