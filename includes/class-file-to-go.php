<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://2060digital.com
 * @since      1.0.0
 *
 * @package    File_To_Go
 * @subpackage File_To_Go/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    File_To_Go
 * @subpackage File_To_Go/includes
 * @author     Ryan Benhase <rbenhase@2060digital.com>
 */
class File_To_Go {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      File_To_Go_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'file-to-go';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->define_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-file-to-go-loader.php';

		$this->loader = new File_To_Go_Loader();
	}


	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_hooks() {
		$this->loader->add_action( 'admin_menu', $this, 'add_admin_menu' );			
		$this->loader->add_action( 'init', $this, 'virtual_page' );		
		$this->loader->add_action( 'admin_init', $this, 'file_to_go_settings_init' );
		$this->loader->add_action( 'admin_enqueue_scripts', $this, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_ajax_change_link', $this, 'change_link_ajax' );
		$this->loader->add_action( 'wp_ajax_update_file', $this, 'update_file_ajax' );
	}
	
	
	
	/**
	 * Enqueue necessary scripts.
	 * 
	 * @access public
	 * @return void
	 */
	public function enqueue_scripts() {
  	
  	wp_register_script( 'file-to-go', plugins_url('/js/min/file-to-go-min.js', dirname( __FILE__ ) ), array( 'jquery' ) );
    global $current_screen;
    if ( 'toplevel_page_file_to_go' == get_current_screen() -> id ) {      
        wp_enqueue_media();
        
        $base = get_option( 'file-to-go-base', 'files' );  
        
        wp_localize_script( 
          'file-to-go',
          'Options',
          array( 
            'base' => $base, 
            'siteUrl' => get_site_url(),
          )
        );
        wp_enqueue_script('file-to-go'); 
    }
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    File_To_Go_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

  /**
   * Add plugin options page.
   * 
   * @since 1.0.0
   * @access public
   * @return void
   */
  public function add_admin_menu() {

      add_menu_page(
          'File to Go', 
          'File to Go', 
          'manage_options', 
          'file_to_go', 
          array( $this, 'options_page' ),
          'dashicons-admin-links'
      );
  }
  
  
  /**
   * Add our settings sections & fields.
   * 
   * @since 1.0.0
   * @access public
   * @return void
   */
  function file_to_go_settings_init() { 
  
  	register_setting( 'file-to-go-settings', 'file-to-go-base', array( $this, 'sanitize_base' ) );
  
  	add_settings_section(
  		'file_to_go_settings_section', 
  		'Settings',
  		array( $this, 'file_to_go_settings_section_callback' ), 
  		'file-to-go-settings'
  	);
  
  	add_settings_field( 
  		'file-to-go-base', 
      'Link Base',
  		array( $this, 'file_to_go_base_render' ), 
  		'file-to-go-settings', 
  		'file_to_go_settings_section' 
  	);

  }
  
  function sanitize_base( $input ) {
    return sanitize_title( $input, 'files' );
  }
  
  
  function file_to_go_base_render() { 
  
  	$base = get_option( 'file-to-go-base', 'files' );
  	?>
  	<?php echo get_site_url(); ?>/<input type='text' name='file-to-go-base' value='<?php echo $base; ?>'>/
  	<?php  
  }
  
  
  function file_to_go_settings_section_callback() { 
    $base = get_option( 'file-to-go-base', 'files' );
    ?>
    <p><strong>Current Link Base: </strong> <?php echo get_site_url(); ?>/<?php echo $base; ?></p>
    <p><strong>WARNING: </strong> Changing your link base will instantly change all of the links listed above, meaning that <br>
    you would need to check your site for any instances of the links above and carefully replace each one.<br>
    Otherwise, you will end up with broken links.</p>
    <p>It's therefore not recommended that you change your link base once it's set.</p>
    <p><a class="do-anyway" href="#">Do it anyway.</a></p>
    <?php  
  }

      
  
  /**
   * Display File to Go options/settings page.
   * 
   * I would normally suggest using the WP Settings API for
   * all options, but due to the unusual nature of this page
   * (and the need to use AJAX), it's only used for the 
   * "traditional" settings at the bottom.
   *
   * @since 1.0.0
   * @access public
   * @return void
   */
  function options_page() { 
    
    $urls = get_option( 'file-to-go-urls' );
    if ( empty( $urls ) )
      $urls = array();
    
    $urls = array_reverse( $urls );
    $base = get_option( 'file-to-go-base', 'files' );  	
    
  	?>		
		<h2>File to Go</h2>  		
		  		  
	  <table cellspacing="10">
		  <thead>
  		  <tr>
    		  <th align="left">File</th>
    		  <th align="left">Link</th>
    		  <th></th>
  		  </tr>
		  </thead>
		  <tbody>
  		  
  		  <?php if ( !empty( $urls ) ) foreach( $urls as $slug => $file ): ?>
    		<tr>
    		  <td class="file">
      		  <a class="original-file" target="_blank" href="<?php echo $file;?>"><?php echo basename( $file );?></a>
      		</td>
      		
    		  <td class="link">
      		  <div class="display-link" style="display:none;">
        		  <a target="_blank" href="<?php echo get_site_url(); ?>/<?php echo $base; ?>/<?php echo $slug; ?>">
          		  <?php echo get_site_url(); ?>/<?php echo $base; ?>/<?php echo $slug; ?>
          		</a>
      		  </div>
      		  <div class="edit-link">
        		  <?php echo get_site_url(); ?>/<?php echo $base; ?>/<input type="text" class="link-slug" name="file-to-go-urls[<?php echo $file;?>]" value="<?php echo $slug;?>">
        		</div>
      		</td>
      		
    		  <td class="actions">
      		  <a data-saved-value="<?php echo $slug; ?>" class="edit-link-slug button-secondary">Change Link</a>
      		  <a data-id="<?php echo $slug; ?>" class="replace-existing-file button-secondary">Replace File</a>
    		  </td>
  		  </tr>
  		  <?php endforeach; ?>
  		  
  		  <tr>
    		  <td><a class="original-file"></a><a href="#" class="button-secondary files-to-go-add-file">Add a New File/Link</a><br></td>
    		  <td class="add-file-container link">
            <div class="display-link" style="display:none;">
        		  <a target="_blank"></a>
      		  </div>
      		  <div class="edit-link">
        		  <input type="text" class="link-slug">
        		</div>
    		  </td>
    		  <td class="actions"></td>
  		  </tr>
  		  
		  </tbody>
	  </table> 
	  <hr>
	  <?php if ( current_user_can( 'manage_options' ) ): ?>
		<form action='options.php' method='post'>
  		<?php
  		settings_fields( 'file-to-go-settings' );
  		do_settings_sections( 'file-to-go-settings' );
  		submit_button();
  		?>
  	</form>
  	<?php endif;
  }
  
  
  /**
   * Update a file via AJAX, echo JSON-encoded response
   * 
   * @since 1.0.0
   * @access public
   * @return void
   */
  public function update_file_ajax() {    
    
    header( 'Content-Type: application/json' );
    
    if ( isset( $_POST['slug'] ) && ( $_POST['fileUrl'] ) ) {
      
      $urls = get_option( 'file-to-go-urls' );
      
      $file = sanitize_url( $_POST['fileUrl'] );
      $slug = sanitize_title_with_dashes( $_POST['slug'] ); 	

      $urls[$slug] = $file;

      update_option( 'file-to-go-urls', $urls );
      
      echo json_encode(
        array(
          "success" => true,
          "message" => "The file has been updated.",
          "file" => $file,
          "slug" => $slug
        )
      );   
      exit;

    }
    echo json_encode(
      array(
        "success" => false,
        "message" => "Data incomplete."
      )
    );   
    exit;
  }
  
  
  /**
   * Change a link via AJAX, echo JSON-encoded response.
   * 
   * @since 1.0.0
   * @access public
   * @return void
   */
  public function change_link_ajax() {    
    
    header( 'Content-Type: application/json' );
    
    if ( isset( $_POST['newSlug'] ) && ( $_POST['file'] ) ) {
      
      $urls = get_option( 'file-to-go-urls' );
      $base = get_option( 'file-to-go-base', 'files' );  	

      $original_value = ( isset( $_POST['originalValue'] ) ? $_POST['originalValue'] : '' );
      $slug = $_POST['newSlug'];
      $file = $_POST['file'];
      
      // Prevent duplicates
      if ( in_array( $slug, array_keys( $urls ) ) && $slug != $original_value ) {
        echo json_encode(
          array(
            "success" => false,
            "message" => "This link is a duplicate. Please choose a different one."
          )
        );   
        exit;
      }
      
      // If we have what we need to update
      if ( !empty( $file ) && !empty( $slug ) ){  
        $file = sanitize_url( $file );
        $slug = sanitize_title_with_dashes( $slug );
        $urls[$slug] = $file; 
        
        // Remove old entry        
        unset( $urls[$original_value] );
        
        update_option( 'file-to-go-urls', $urls );
        
        echo json_encode(
          array(
            "success" => true,
            "message" => "Link updated.",
            "file" => $file,
            "slug" => $slug
          )
        );
      } else {
        echo json_encode(
          array(
            "success" => false,
            "message" => "Empty values found. Make sure you've specified a link."
          )
        );
      }
      exit;            
    }  
    echo json_encode(
      array(
        "success" => false,
        "message" => "Data incomplete."
      )
    );   
    exit;
  }
  
  /**
   * Create a "virtual page" to do our redirecting.
   * 
   * @since 1.0.0
   * @access public
   * @return void
   */
  public function virtual_page() {
	  
  	$url = trim( parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH ), '/' );  	
  	$base = get_option( 'file-to-go-base', 'files' );  	
		$parts = explode( '/', $url );
		
		if ( $parts[0] == $base ){
			$key = $parts[1];	 
	 			
			$urls = get_option( 'file-to-go-urls' );
			
			if ( isset( $urls[$key] ) && !empty( $urls[$key] ) ) {

  			wp_redirect( $urls[$key] );
  			exit;
			} 									
		}       
  }
}
?>